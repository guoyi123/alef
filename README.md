# alef

## 介绍
Admin LTE External Framework(alef)是将Admin LTE这款基于Bootstrap的后台管理系统的外部框架抽离出，并且加以修改而得到的一个后台管理系统外部框架，只要将对应功能的页面嵌入在index中的iframe即可。

## 效果

![page](picture/page.png)